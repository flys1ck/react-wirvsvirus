# WirVsVirus Simulation

## Quick Start: Development on locale Machine

### System Prerequisites

To develop on this project it is recommended to have the following dependencies installed:

- **Node.js**

All commands get executed in the root directory of the project.

### Frontend

The projects frontend is developed in **Javascript** using the framework **React**. All frontend development files are located in `src/`. To develop only the parts of the frontend, which do not require any API calls, use the following commands:

```bash
npm install
# in development mode
npm run start:dev
# in production mode
npm run start:prod
```

This will install all necessary dependencies and run **webpack**, which bundles all Javascript files in a single file called `bundle.js`. The `bundle.js` and `index.html` (which both get deployed) are located in `src/resources`. The webserver is available under `localhost:9000` and has live reload enabled.

## CI/CD Pipeline

The pipeline consists of three stages: Build, Test and Deploy.

- **Build**: Builds the application using `npm`
- **Deploy**: Deploys the application depending on the performed git operation to Heroku. See [Deployment on Heroku](#Deployment) for further information on available Heroku applications.

On every **commit to master** the Build and Deploy stage will be executed.

## Deployment on Heroku

This application is replicated on Heroku:

- [wirvsvirus-simulation](wirvsvirus-simulation.herokuapp.com): snapshot of the current master