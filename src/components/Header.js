import React from "react";
// FortAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// Router
import { NavLink } from "react-router-dom";
// i18next
import { withTranslation } from "react-i18next";

class Header extends React.Component {
    render() {
        const { t } = this.props;
        return (
            <nav className="navbar navbar-dark sticky-top navbar-expand-md bg-dark shadow">
                <NavLink className="navbar-brand" to="/">
                    <FontAwesomeIcon className="mx-2 text-theme-orange" icon="allergies" size="lg" fixedWidth />
                    COVID-Sim
                </NavLink>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
                            <NavLink className="nav-link" to="/home">
                                Information
                            </NavLink>
                        </li>
                        <li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
                            <NavLink className="nav-link" to="/simulation">
                                {t("simulation")}
                            </NavLink>
                        </li>
                        <li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
                            <NavLink className="nav-link" to="/settings">
                                {t("settings")}
                            </NavLink>
                        </li>
                    </ul>
                    <ul className="navbar-nav ml-auto"></ul>
                </div>
            </nav>
        );
    }
}

export default withTranslation()(Header);
