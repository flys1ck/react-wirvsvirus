import React from "react";
// i18next
import { withTranslation } from "react-i18next";
// Components
import { Heading } from "./util/Heading";
import { ContainerFluidPadding } from "./util/ContainerFluidPadding";
import { NewsArticle } from "./util/NewsArticle";

class Home extends React.Component {
    render() {
        const { t } = this.props;
        return (
            <React.Fragment>
                <Heading>Alles Wichtige zu COVID-19</Heading>
                <ContainerFluidPadding>
                    <NewsArticle
                        title="Was muss ich über COVID-19 wissen?"
                        timestamp={1584887480000}
                        author="PuzzleWeb"
                    >
                        <b>Was ist COVID-19 (das “Corona-Virus”)?</b>
                        <p className="text-justify mb-1">
                            COVID-19 (Kurzform für Corona-Virus-Disease-19) ist eine Atemwegserkrankung mit teilweise
                            schweren Folgen die zum ersten Mal im Dezember 2019 in der chinesischen Provinz Hubei
                            nachgewiesen wurde. In den letzten drei Monaten hat sich die Krankheit rasant auf der ganzen
                            Welt, auch in Deutschland ausgebreitet.
                        </p>
                        <b>Wie wird COVID-19 übertragen?</b>
                        <ul className="mb-2">
                            <li>
                                <b>Tröpfcheninfektion</b>
                                <br />
                                Beim Husten, Niesen oder auch Sprechen werden viele kleine Tröpfchen in die Umgebung
                                abgegeben. Wenn sich dabei in der Nähe (bis zu 2 Meter entfernt) eine andere Person
                                aufhält, kann diese die Tröpfchen einatmen und den Erreger somit in sich aufnehmen
                            </li>
                            <li>
                                <b>Schmierinfektion</b>
                                <br />
                                Dabei kommt der Erreger in Kontakt mit einem Gegenstand (beispielsweise weil ein kranker
                                Mensch auf einen diesen geniest hat oder hustet). Fasst nun eine andere Person die
                                infizierte Oberfläche an und berührt mit der Hand danach das Gesicht, so können auch auf
                                diesem Wege SARS-CoV-II Viren in den Körper gelangen.
                            </li>
                        </ul>
                        <p className="text-justify mb-1">
                            <b>Wie kann ich mich schützen?</b>
                            <br />
                            In Anbetracht der rasant ansteigenden Fallzahlen ist <b>“Social Distancing"</b> unsere
                            größte Chance! Der bewusste Verzicht auf unser Sozialleben ist nicht leicht, doch momentan
                            müssen wir alle ein Stück unserer Freiheit aufgeben, um uns gemeinsam dem Virus zu stellen.{" "}
                        </p>
                        <p className="text-justify mb-1">
                            <b>
                                In unserer COVID-Sim kannst du selbst nachvollziehen, welche Auswirkungen der bewusste
                                Verzicht auf unser Sozialleben hat und wie wir damit die Ausbreitung von COVID-19
                                verlangsamen können.
                            </b>
                        </p>
                        Weitere Schutzmaßnahmen:
                        <ul className="mb-2">
                            <li>
                                <b>Auf eine gute Handhygiene achten!</b>
                                <br />
                                Regelmäßiges, gründliches Händewaschen ist ein Muss. Die World-Health-Organization zeigt
                                euch in diesem{" "}
                                <a href="https://www.youtube.com/watch?v=3PmVJQUCm4E">Händewasch-Video</a> wie es geht.
                            </li>
                            <li>
                                <b>Abstand zu anderen Personen halten!</b>
                                <br />
                                Ein Mindestabstand von 1 - 2 m sollte dringend eingehalten werden um eine
                                Tröpfcheninfektion zu vermeiden
                            </li>
                            <li>
                                <b>Nicht mit den Händen das Gesicht berühren!</b>
                                <br />
                                COVID-19 kann auch durch den Kontakt mit infizierten Oberflächen übertragen werdeen.
                                Deshalb achte darauf, dass du wenn du beispielsweise im Supermarkt einkaufst,du keine
                                Gegenstände oder Waren und anschließend dein Gesicht berührst. Eine Atemschutzmaske,
                                aber auch einfach ein Schal über Mund und Nase schützt dich zwar nicht direkt vor den
                                Viren, kann dir aber dabei helfen aufmerksamer zu sein und ein unbewusstes Berühren des
                                Gesichts zu vermeiden.
                            </li>
                        </ul>
                        <p className="text-justify mb-2">
                            <b>Wie anstecked ist COVID-19?</b>
                            <br />
                            Um zu beurteilen wie ansteckend eine Krankheit ist, muss die sogenannte
                            “Basisreproduktionsrate”, auch R0 genannt bekannt sein. Diese Zahl gibt an, wie viele
                            Personensich durchschnittlich bei einem Erkrankten anstecken. Im Fall von Covid-19 liegt
                            Basisreproduktionsrate zwischen 2,4 und 3,3. Das bedeutet, jeder mit SARS-CoV-II infizierter
                            Menschen, ungefähr 2 - 3 andere Personen anstecken wird.
                        </p>
                        <p className="text-justify mb-2">
                            <b>Welche Symptome treten bei COVID 19 auf?</b>
                            <br />
                            Typische Symptome von COVID-19 sind <b>Fieber, Husten, Schnupfen und Halsschmerzen</b>, in
                            manchen Fällen jedoch auch <b>Atemnot und schwere Lungenentzündungen</b>. Für manche, vor
                            allem jüngere Leute fühlt sich die Krankheit nur an wie eine lästige Erkältung, doch andere
                            erleben schwere Symptome und müssen im Krankenhaus behandelt werden. In einigen Fällen kann
                            COVID-19 auch zum Tod führen.
                        </p>
                        <p className="text-justify mb-2">
                            Der Krankheitsverlauf kann sehr unterschiedlich sein und hängt von vielen Faktoren ab. Vor
                            allem das Alter des Infizierten spielt eine Rolle und ob dieser gesundheitlich vorbelastet
                            ist. Das Robert-Koch-Institut geht davon aus, dass Menschen ab 50 bis 60 Jahren ein erhöhtes
                            Risiko für einen schweren Krankheitsverlauf haben. Auch Menschen mit koronarer
                            Herzerkrankung, Lungenerkrankungen (z.B. Asthma), Diabetes mellitus, Krebs oder
                            Immunschwäche sind besonders in Gefahr.
                        </p>
                        <p className="text-justify mb-1">Wir wünschen viel Spaß beim Ausprobieren!</p>
                    </NewsArticle>
                </ContainerFluidPadding>
            </React.Fragment>
        );
    }
}

export default withTranslation()(Home);
