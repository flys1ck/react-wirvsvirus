import React from "react";
// i18next
import { withTranslation } from "react-i18next";
// Components
import { Heading } from "./util/Heading";
import LanguageSelect from "./util/SettingsLanguageSelect";
import { SettingsSection } from "./util/SettingsSection";
import CardInfo from "./util/CardInfo";
import { ContainerFluidPadding } from "./util/ContainerFluidPadding";

class Settings extends React.Component {
    render() {
        const { t } = this.props;
        return (
            <React.Fragment>
                <Heading>{t("settings")}</Heading>
                <ContainerFluidPadding>
                    <CardInfo>
                        <p className="mb-0 text-justify">{t("appNotFullyTranslatedNotice")}</p>
                    </CardInfo>
                    <SettingsSection title={t("displaySettings")}>
                        <LanguageSelect />
                    </SettingsSection>
                </ContainerFluidPadding>
            </React.Fragment>
        );
    }
}

export default withTranslation()(Settings);
