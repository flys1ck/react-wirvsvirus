import React from "react";
// i18next
import { withTranslation } from "react-i18next";
// Simulation
import { simulate } from "./simulation/main.js";
// Components
import { Heading } from "./util/Heading";
import { ContainerFluidPadding } from "./util/ContainerFluidPadding";
import CardInfo from "./util/CardInfo";

class Simulation extends React.Component {
    componentDidMount() {
        simulate();
    }

    render() {
        const { t } = this.props;
        return (
            <React.Fragment>
                <Heading>Simulation</Heading>
                <ContainerFluidPadding>
                    <canvas className="border bg-dark rounded p-2" id="simulation" width="800" height="600"></canvas>
                    <CardInfo className="text-justify">
                        COVID-Sim zeigt wie sich COVID-19 in einer kleinen Stadt verbreitet. Nach und nach infizieren
                        sich die Bewohner bei ihren Alltagsaktivitäten mit dem Erreger SARS-CoV-II und übertragen ihn an
                        ihre Mitmenschen. <br />
                        <br />
                        <div className="mb-2">
                            <b>Bewohner</b>
                            <br />
                            Die kleinen Kreise stellen die Bewohner einer Kleinstadt dar. Ihre farbige Markierung gibt
                            den Gesundheitszustand der Person an:
                            <br />
                            <ul>
                                <li>
                                    <b>Hellgrün:</b> gesund
                                </li>
                                <li>
                                    <b>Orange:</b> mit COVID-19 infiziert, aber noch nicht ansteckend und ohne Symptome
                                </li>
                                <li>
                                    <b>Dunkelrot:</b> mit COVID-19 infiziert, ansteckend und ohne Symptome
                                </li>
                                <li>
                                    <b>Violet:</b> mit COVID-19 infiziert und mit Symptomen (diese Personen fühlen sich
                                    sehr krank und bleiben zu Hause)
                                </li>
                                <li>
                                    <b>Cyan:</b> Personen die COVID-19 hatten und jetzt wieder geheilt sind
                                </li>
                                <li>
                                    <b>Totenkopf:</b> verstorbene Personen
                                </li>
                            </ul>
                        </div>
                        <div className="mb-2">
                            <b>Gebäude</b>
                            <br />
                            Es gibt zwei Arten von Gebäuden:
                            <ul>
                                <li>
                                    <b>Blaues Dach:</b> Häuser in denen die Bewohner leben
                                </li>
                                <li>
                                    <b>Rotes Dach:</b> Orte des öffentlichen Lebens, wie beispielsweise Schulen, Kinos,
                                    Restaurants, etc.
                                </li>
                            </ul>
                        </div>
                        <div className="mb-2">
                            <b>Ablauf</b>
                            <br />
                            Morgens verlassen die Bewohner ihre Häuser (blaue Dächer) um ihren Alltagsaktivitäten
                            nachzugehen. <br />
                            Dabei suchen sie bevorzugt <b>Orte des öffentlichen Lebens</b> (rote Dächer) auf. Am Ende
                            eines Tages kehren sie nach Hause zurück. And den Orten des öffentlichen Lebens ist die
                            Wahrscheinlichkeit sich mit COVID-19 zu infizieren besonders groß, da hier besonders viele
                            Menschen anzutreffen sind. <br />
                            Wenn sich Leute infizieren, dann sind sie für eine gewisse Zeit noch nicht anstecked{" "}
                            <b>(orange)</b>.<br />
                            Bald jedoch werden diese Infizierten ansteckend, da sie jedoch noch keine Symptome zeigen{" "}
                            <b>(dunkelrot)</b> bewegen sie sich trotzdem im öffentlichen Raum und stecken somit andere
                            Menschen an! Bei Covid-19 steckt jeder Infizierter zwischen 2 und 3 weitere Personen an.
                            Zeigen die Infizierten dann Symptome bleiben sie zu Hause um zu genesen <b>(violet)</b>.
                            Nach einer vollständig abgeheilten COVID-19 Erkrankung sind die meisten Personen immun{" "}
                            <b>(cyan)</b>.
                        </div>
                    </CardInfo>
                </ContainerFluidPadding>
            </React.Fragment>
        );
    }
}

export default withTranslation()(Simulation);
