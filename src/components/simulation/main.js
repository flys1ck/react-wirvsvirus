import Person from "./util/Person.js";
import PointOfInterest from "./util/PointOfInterest.js";
import { getRandomInt } from "./util/utility.js";

// canvas
var canvas;
var canvasContext;
// assets
var assets = {};
var assetSources = {
    background: "./assets/background_dark.png",
    house: "./assets/house_blue.png",
    poi: "./assets/house_big.png",
    skull: "./assets/skull.png"
};
const poiWidth = 32;
const poiHeight = 40;
const houseRadius = 32; // px size of house.png
const skullRadius = 16;
// animation
const framesPerSecond = 20;
// time
var day = 0;
const stopDay = 60; // [day 0, stopDay)
const timeStepsPerDay = 130;
var timeStep = 0;
// board
const width = 800;
const height = 600;
// grid
const gridBoxWidth = 20; // should be a divisor of width and height
// person
var personList = [];
const personAmount = 30;
const personInfectedAmount = 1; // persons, who are infected at start
const personRadius = 10;
const personRandomMovePercentage = 0.05;
// poi
var poiList = [];
const poiAmount = 5;
// infection (in days)
const probabilityToGetInfected = 0.04;
const probabilityToDie = 0.05;
const timeToGetInfectious = 4; // calulated from start
const timeToGetSymptomps = 5; // calculated from start
const timeToRecover = 13; // calculated from start
// colors
const boardColor = "#757780";
const gridColor = "#F1FFFA";
var personColor;
const personHealthyColor = "#3BE267";
const personInfectedColor = "#F98D45";
const personInfectiousColor = "#6B0000";
const personSymptompsColor = "#9E0079";
const personRecoveredColor = "#1761AA";

export function simulate() {
    canvas = document.getElementById("simulation");
    canvasContext = canvas.getContext("2d");

    // load assets
    loadAssets();
    // init objects
    initPointOfInterest();
    initPersons();

    // time ticks
    var intervalId = setInterval(function() {
        // new day cycle
        if (timeStep++ % timeStepsPerDay == 0) {
            // check state of infection and set dest to random POI
            for (let person of personList) {
                // check state of infection
                checkInfectionState(person);
                // get dest POI of person (for day)
                var destPoi = poiList[Math.floor(Math.random() * poiList.length)];
                person.setDest(destPoi.posX, destPoi.posY);
            }
            // update values
            timeStep = 1;
            day++;
            // stop simulation
            if (day == stopDay) {
                clearInterval(intervalId);
            }
        }
        infect();
        drawBoard();
        movePersons();
    }, 1000 / framesPerSecond);
}

function loadAssets() {
    for (let src in assetSources) {
        assets[src] = new Image();
        assets[src].src = assetSources[src];
    }
}

function initPointOfInterest() {
    for (var pointOfInterestId = 0; pointOfInterestId < poiAmount; pointOfInterestId++) {
        // randomly get pos in grid
        var posX = getRandomInt(1, width / gridBoxWidth) * gridBoxWidth;
        var posY = getRandomInt(1, height / gridBoxWidth) * gridBoxWidth;
        // init poi
        var poi = new PointOfInterest(pointOfInterestId, posX, posY);
        poiList.push(poi);
    }
}

function initPersons() {
    for (var personId = 0; personId < personAmount; personId++) {
        // randomly get pos in grid
        var posX = getRandomInt(1, width / gridBoxWidth) * gridBoxWidth;
        var posY = getRandomInt(1, height / gridBoxWidth) * gridBoxWidth;
        // init persons
        var person = new Person(personId, posX, posY);
        // patient 0
        if (personId < personInfectedAmount) {
            person.infect(day);
        }
        personList.push(person);
    }
}

function drawBoard() {
    // background
    // canvasContext.fillStyle = boardColor;
    // canvasContext.fillRect(0, 0, canvas.width, canvas.height);
    canvasContext.drawImage(assets.background, 0, 0);

    // grid
    // for (var x = 0; x <= width; x += gridBoxWidth) {
    //     canvasContext.moveTo(x, 0);
    //     canvasContext.lineTo(x, height);
    // }

    // for (var x = 0; x <= height; x += gridBoxWidth) {
    //     canvasContext.moveTo(0, x);
    //     canvasContext.lineTo(width, x);
    // }
    // canvasContext.strokeStyle = gridColor;
    // canvasContext.stroke();
    // poi
    for (let poi of poiList) {
        canvasContext.drawImage(assets.poi, poi.posX - poiWidth / 2, poi.posY - poiHeight / 2);
    }
    // person
    for (let person of personList) {
        canvasContext.drawImage(assets.house, person.homePosX - houseRadius / 2, person.homePosY - houseRadius / 2);

        // person
        // check infection sate (check top down)
        if (person.isDead) {
            canvasContext.drawImage(assets.skull, person.posX - skullRadius / 2, person.posY - skullRadius / 2);
        } else {
            if (person.isRecovered) {
                personColor = personRecoveredColor;
            } else if (person.hasSymptomps) {
                personColor = personSymptompsColor;
            } else if (person.isInfectious) {
                personColor = personInfectiousColor;
            } else if (person.isInfected) {
                personColor = personInfectedColor;
            } else {
                personColor = personHealthyColor;
            }
            canvasContext.fillStyle = personColor;
            canvasContext.fillRect(
                person.posX - personRadius / 2,
                person.posY - personRadius / 2,
                personRadius,
                personRadius
            );
        }
    }
}

function _movePerson(person) {
    // don't move if person is at dest
    if (!person.isRecovered && (person.isAtDest || person.hasSymptomps)) {
        return;
    }

    if (Math.random() < personRandomMovePercentage) {
        // move randomly
        person.moveRandom(gridBoxWidth);
    } else {
        person.moveToDest(gridBoxWidth);
    }
}

function movePersons() {
    if (timeStep % (timeStepsPerDay + 1) <= timeStepsPerDay / 3) {
        // first third of day -> move to POI
        for (let person of personList) {
            _movePerson(person);
        }
    } else if (timeStep % (timeStepsPerDay + 1) <= 2 * (timeStepsPerDay / 3)) {
        // second third of day -> move to POI
        for (let person of personList) {
            // set destination to home
            person.setDest(person.homePosX, person.homePosY);
            _movePerson(person);
        }
    } else {
        // last third of day -> move to home
        for (let person of personList) {
            _movePerson(person);
        }
    }
}

function infect() {
    for (let personA of personList) {
        // infected and recovered persons can't infect
        if (!personA.isInfectious || personA.isRecovered) {
            continue;
        }
        for (let personB of personList) {
            // infected people can't get infected again
            if (personB.isInfected || personB.isRecovered) {
                continue;
            }
            // infect, if person is in the same position
            if (personA.posX == personB.posX && personA.posY == personB.posY) {
                if (Math.random() < probabilityToGetInfected) {
                    personB.infect(day);
                }
            }
        }
    }
}

function checkInfectionState(person) {
    if (person.dayOfInfection != undefined) {
        if (day - person.dayOfInfection == timeToGetInfectious) {
            person.isInfectious = true;
        } else if (day - person.dayOfInfection == timeToGetSymptomps) {
            person.hasSymptomps = true;
        } else if (day - person.dayOfInfection == timeToRecover) {
            // person can recover or die
            if (Math.random() < probabilityToDie) {
                person.isDead = true;
            } else {
                person.isRecovered = true;
            }
        }
    }
}
