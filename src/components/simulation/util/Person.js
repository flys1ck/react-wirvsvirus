import { getRandomInt } from "./utility.js";

export default class Person {
    constructor(id, posX, posY, destX, destY) {
        this.id = id;
        // age of person
        this.age =
            // home position
            this.homePosX = posX;
        this.homePosY = posY;
        // position
        this.posX = this.homePosX;
        this.posY = this.homePosY;
        // destination (POI or Home), changes each day
        this.isAtDest = false;
        this.destX = destX;
        this.destY = destY;
        // state of infection
        this.dayOfInfection;
        this.isRecovered = false; // recovered persons can't be reinfected
        this.isInfected = false; // person has infection, but can't infect
        this.isInfectious = false; // person has infection and can infect
        this.hasSymptoms = false; // person stays at home
        this.isDead = false;
    }

    setPos(newPosX, newPosY) {
        this.posX = newPosX;
        this.posY = newPosY;
    }

    setDest(newDestX, newDestY) {
        this.isAtDest = false;
        this.destX = newDestX;
        this.destY = newDestY;
    }

    moveRandom(gridBoxWidth) {
        /*
         * 0 - left
         * 1 - up
         * 2 - right
         * 3 - down
         */
        var direction = getRandomInt(0, 4);
        switch (direction) {
            case 0:
                this.posX -= gridBoxWidth;
                break;
            case 1:
                this.posY += gridBoxWidth;
                break;
            case 2:
                this.posX += gridBoxWidth;
                break;
            case 3:
                this.posY -= gridBoxWidth;
                break;
        }
    }

    moveToDest(gridBoxWidth) {
        if (this.posX == this.destX && this.posY == this.destY) {
            // person is at destX and destY -> stay there
            this.isAtDest = true;
            return;
        } else {
            this.isAtDest = false;
        }

        if (this.posX == this.destX) {
            // person is at destX -> moves in Y direction
            this.posY += Math.sign(this.destY - this.posY) * gridBoxWidth;
        } else if (this.posY == this.destY) {
            // person is at destY -> moves in X direction
            this.posX += Math.sign(this.destX - this.posX) * gridBoxWidth;
        } else {
            // randomly decide if we move in X or Y
            if (Math.round(Math.random())) {
                // move in X
                this.posX += Math.sign(this.destX - this.posX) * gridBoxWidth;
            } else {
                // move in Y
                this.posY += Math.sign(this.destY - this.posY) * gridBoxWidth;
            }
        }
    }

    infect(day) {
        this.isInfected = true;
        this.dayOfInfection = day;
    }
}
