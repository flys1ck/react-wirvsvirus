export default class PointOfInterest {
    constructor(id, posX, posY) {
        this.id = id;
        // position
        this.posX = posX;
        this.posY = posY;
    }
}
