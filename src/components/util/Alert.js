import React from "react";
import PropTypes from "prop-types";

export class Alert extends React.Component {
    render() {
        const altertContext = this.props.alertContext;
        return (
            <div
                className={"alert alert-" + altertContext + " alert-dismissible fade show fixed-bottom mx-2"}
                role="alert"
            >
                {this.props.children}
                <button type="button" className="close" onClick={this.props.onAlertClose}>
                    <span>&times;</span>
                </button>
            </div>
        );
    }
}

Alert.defaultProps = {
    alertContext: "primary"
};

Alert.propTypes = {
    // alertContext expects Bootstrap contextual classes
    alertContext: PropTypes.string.isRequired,
    onAlertClose: PropTypes.func.isRequired
};
