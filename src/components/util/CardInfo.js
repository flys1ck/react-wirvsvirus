import React from "react";
// i18next
import { withTranslation } from "react-i18next";
// FortAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class CardInfo extends React.Component {
    render() {
        const { t } = this.props;
        return (
            <div className="card border-theme-orange mb-3">
                <div className="card-header text-theme-orange">
                    <FontAwesomeIcon className="mr-2" icon="info" fixedWidth />
                    <b>{t("informationCalloutHeader")}</b>
                </div>
                <div className="card-body">{this.props.children}</div>
            </div>
        );
    }
}

export default withTranslation()(CardInfo);
