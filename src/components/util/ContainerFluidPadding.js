import React from "react";

export class ContainerFluidPadding extends React.Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-1 d-none d-lg-flex"></div>
                    <div className="col d-block d-flex-lg">{this.props.children}</div>
                    <div className="col-1 d-none d-lg-flex"></div>
                </div>
            </div>
        );
    }
}
