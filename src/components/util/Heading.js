import React from "react";

export class Heading extends React.Component {
    render() {
        return (
            <React.Fragment>
                <h3 className="text-center">{this.props.children}</h3>
                <hr />
            </React.Fragment>
        );
    }
}
