import React from "react";

export class Main extends React.Component {
    render() {
        return <main className="flex-fill bg-light py-2">{this.props.children}</main>;
    }
}
