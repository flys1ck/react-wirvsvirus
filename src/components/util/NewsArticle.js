import React from "react";

export class NewsArticle extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <article className="border rounded bg-white my-2 p-3">
                    <div className="mb-2">
                        <h5 className="text-info font-weight-bold mb-0">{this.props.title}</h5>
                        <small className="text-muted">{new Date(this.props.timestamp).toLocaleString()}</small>
                    </div>
                    {this.props.children}
                    <div className="text-right font-weight-bold font-italic">
                        <small className="font-weight-bold font-italic"> - {this.props.author}</small>
                    </div>
                </article>
            </React.Fragment>
        );
    }
}
