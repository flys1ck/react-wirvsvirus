import React from "react";
import { useTranslation } from "react-i18next";

const SettingsLanguageSelect = () => {
    const { t, i18n } = useTranslation();
    const languages = Object.keys(i18n.options.resources);
    return (
        <React.Fragment>
            <div className="form-group">
                <label htmlFor="languageSelect">{t("languageSelect")}</label>
                <select
                    className="form-control"
                    id="languageSelect"
                    value={i18n.language}
                    onChange={() => i18n.changeLanguage(event.target.value)}
                >
                    {languages.map(language => (
                        <option key={language} value={language}>
                            {t(language)}
                        </option>
                    ))}
                </select>
                <small className="form-text text-muted">{t("languageSelectInfo")}</small>
            </div>
        </React.Fragment>
    );
};

export default SettingsLanguageSelect;
