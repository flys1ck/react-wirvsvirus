import React from "react";

export class SettingsSection extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <h6 className="text-theme-orange">
                    <b>{this.props.title}</b>
                </h6>
                {this.props.children}
            </React.Fragment>
        );
    }
}
