import React from "react";

// FortAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class LoadingSpinner extends React.Component {
    render() {
        return (
            <div className="text-center">
                <FontAwesomeIcon className="text-theme-orange" icon="circle-notch" size="2x" spin fixedWidth />
            </div>
        );
    }
}
