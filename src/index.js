import React from "react";
import { render } from "react-dom";

// Bootstrap
import "bootstrap";
// FortAwesome
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
library.add(fas, fab);
// Router
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
// Sass
import "./index.scss";
// i18n
import "./tools/i18n";
// Components
import Header from "./components/Header";
import Home from "./components/Home";
import Simulation from "./components/Simulation";
import { Main } from "./components/util/Main";
import Settings from "./components/Settings";

// Environment gets checked in i18n module

class App extends React.Component {
    render() {
        return (
            <Router>
                <Header />
                <Main>
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/home" />
                        </Route>
                        <Route path="/home">
                            <Home />
                        </Route>
                        <Route path="/simulation">
                            <Simulation />
                        </Route>
                        <Route path="/settings">
                            <Settings />
                        </Route>
                    </Switch>
                </Main>
            </Router>
        );
    }
}

render(<App />, window.document.getElementById("root"));
