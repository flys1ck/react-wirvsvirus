// i18next
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";

// Check environment and set flags
var isDevelopment;
if (process.env.NODE_ENV !== "production") {
    console.log("App is running in development mode!");
    isDevelopment = true;
} else {
    console.log("App is running in production mode!");
    isDevelopment = false;
}

const de = {
    simulation: "Simulation",
    settings: "Einstellungen",
    home: "Home",
    languageSelect: "Sprache",
    de: "Deutsch",
    en: "Englisch",
    displaySettings: "Anzeigeeinstellungen",
    informationCalloutHeader: "Information",
    refresh: "Aktualisieren",
    languageSelectInfo: "Wähle die Sprache deiner Anwendung",
    appNotFullyTranslatedNotice: "Teile dieser App sind noch nicht vollständig in andere Sprachen übersetzt.",
    date: "Datum"
};

const en = {
    simulation: "Simulation",
    settings: "Settings",
    languageSelect: "Language",
    home: "Home",
    de: "German",
    en: "English",
    displaySettings: "Display Settings",
    languageSelectInfo: "Select the language of your application",
    appNotFullyTranslatedNotice:
        "This app is currently not fully translated. Please use the german version for complete translations."
};

i18n.use(initReactI18next)
    .use(LanguageDetector)
    .init({
        fallbackLng: ["en", "de"],
        resources: {
            // sort resources alphabetically (order they are displayed in options)
            de: {
                translation: de
            },
            en: {
                translation: en
            }
        },
        debug: isDevelopment,
        load: "languageOnly"
    });

export default i18n;
