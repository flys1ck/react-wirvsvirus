const path = require("path");
const merge = require("webpack-merge");
const common = require("./webpack.common.js");

const DIST_DIR = path.resolve(__dirname, "dist");

module.exports = merge(common, {
    mode: "development",
    devtool: "inline-source-map",
    devServer: {
        port: 9000,
        historyApiFallback: true,
        inline: true,
        hot: true,
        contentBase: DIST_DIR
    }
});
